import React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import configureStore from "redux-mock-store";
import Samples from "../../src/presentation/views/samples";

describe("Analysis Component", () => {
  const initialState = {};
  const mockStore = configureStore();
  let store;

  it("renders correctly", () => {
    store = mockStore(initialState);
    const tree = renderer
      .create(
        <Provider store={store}>
          <Samples />
        </Provider>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
