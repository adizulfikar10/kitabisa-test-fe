## Installation

Install the dependencies and devDependencies.

### `yarn or npm i`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br>

### `yarn test:jest`

Snapshot tests to make sure your UI does not change unexpectedly.

## License

Muhamad Adi Zulfikar

email : adizulf@gmail.com
