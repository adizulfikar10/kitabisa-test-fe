import axios from "axios";

console.log(process.env.REACT_APP_API_URL);

const apiUrl =
  process.env.NODE_ENV === "production"
    ? process.env.REACT_APP_API_URL
    : "campaign-service";

export default {
  getAll: async () => {
    const response = await axios.get(
      `${apiUrl}/fe_code_challenge/campaign.json`
    );

    return response.data.data;
  }
};
