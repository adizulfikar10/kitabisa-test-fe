import React from "react";
import "./Campaign.scss";

const Campaign = (props) => {
  return (
    <div className="card p-3">
      <div className="card-border">
        <img className="image" src={props.image} />
        <div className="content p-2 px-4">
          <div>
            <h2 className="text-title">{props.title}</h2>
            {(props.donation_percentage * 100).toFixed(2)}%
          </div>
          <div className="bar mt-2 mb-4 relative">
            <div
              className={`bar absolute ${
                props.donation_percentage * 100 > 100 ? "success" : "progress"
              }`}
              style={{ width: props.donation_percentage * 100 + "%" }}
            />
          </div>
          <div className="flex justify-between">
            <div className="col-6">
              <span>Terkumpul</span>
              <span className="text-donation my-2">
                Rp {props.donation_received.toLocaleString("id-ID")}
              </span>
            </div>
            <div className="col-6 text-right">
              <span>Sisa Hari</span>
              <span className="my-2 text-donation">{props.days_remaining}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Campaign;
