import React from "react";
import "./Button.scss";

const Button = (props) => {
  const handleClick = () => {
    props.click();
  };
  return <button onClick={(e) => handleClick(e)}>{props.title}</button>;
};

export default Button;
