import React from "react";
import { Link } from "react-router-dom";
import Button from "../../components/share/button/Button";

const Main = () => {
  return (
    <div className="col-12 text-center mt-20">
      <Link to="/samples">
        <Button title="View All Campaign" click={(e) => {}} />
      </Link>
    </div>
  );
};

export default Main;
