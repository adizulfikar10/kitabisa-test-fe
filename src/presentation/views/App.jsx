import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import history from "../../infrastructure/history";
import routes from "../../infrastructure/routes";
import Main from "./main";

const App = () => {
  return (
    <BrowserRouter history={history}>
      <Route exact path="/" component={Main} />
      {routes.map(({ title, component: Component, url, exact }) => (
        <Route
          key={url}
          path={url}
          exact={exact}
          render={(compProps) => <Component {...compProps} title={title} />}
        />
      ))}
    </BrowserRouter>
  );
};

export default App;
