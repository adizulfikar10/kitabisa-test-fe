import React from "react";
import Samples from ".";
import Main from "../main";

const SamplesConfig = [
  {
    title: "Sample Route",
    component: () => <Samples />,
    url: "/samples",
    exact: true,
    requirePermission: false
  },
  {
    title: "Sample Detail",
    component: () => <Main />,
    url: "/samples/:id",
    exact: true,
    requirePermission: false
  }
];

export default SamplesConfig;
