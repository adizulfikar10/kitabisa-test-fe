import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadSamples } from "../../../application/actions/samples";
import { getSamples } from "../../../application/selectors/samples";
import Campaign from "../../components/campaign/Campaign";
import Header from "../../components/header/Header";
import Button from "../../components/share/button/Button";

const Samples = () => {
  const [state, setState] = useState(new Date());
  const [campaignList, setCampaignList] = useState([]);
  const [sort, setSort] = useState("");

  const dispatch = useDispatch();
  const campaign = useSelector(getSamples);

  useEffect(() => {
    dispatch(loadSamples);
  }, [dispatch]);

  useEffect(() => {
    setCampaignList(campaign);
  }, [campaign]);

  useEffect(() => {
    const temp = campaignList.sort((a, b) => {
      if (sort === "ASC")
        return (
          b.donation_percentage - a.donation_percentage ||
          b.days_remaining - a.days_remaining
        );
      else
        return (
          a.donation_percentage - b.donation_percentage ||
          a.days_remaining - b.days_remaining
        );
    });

    setCampaignList(temp);
    setState(new Date());
  }, [sort]);

  return (
    <div>
      <Header
        child={[
          <Button
            title="Sort By Donation Goal"
            click={(e) =>
              setSort(sort === "" || sort === "DESC" ? "ASC" : "DESC")
            }
          />
        ]}
      />

      <main>
        <div className="flex flex-between mt-5">
          {campaignList.map((el) => (
            <div key={el.id} className="xs-col-12 sm-col-4 mb-2">
              <Campaign {...el} />
            </div>
          ))}
        </div>
      </main>
    </div>
  );
};

export default Samples;
