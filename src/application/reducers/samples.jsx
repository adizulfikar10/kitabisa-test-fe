import { SET_SAMPLES } from "../actions/samples";

const initialState = {
  sampleDatas: [],
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SAMPLES:
      return { sampleDatas: action.payload, error: null };
    default:
      return state;
  }
};

export default reducer;
