import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
import middleware from "../middleware";
import reducers from "../reducers";

// dev tool
const composeEnhancers =
  (process.env.NODE_ENV === "development" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

export const configureStore = (services) =>
  createStore(
    reducers,
    composeEnhancers(
      applyMiddleware(thunk, ...middleware.map((f) => f(services)))
    )
  );
