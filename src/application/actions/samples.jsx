export const LOAD_SAMPLES = "[samples] load";
export const SET_SAMPLES = "[samples] set";

export const loadSamples = {
  type: LOAD_SAMPLES
};

export const setSamples = (samples) => ({
  type: SET_SAMPLES,
  payload: samples
});
