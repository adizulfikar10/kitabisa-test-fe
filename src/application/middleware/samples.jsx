import { LOAD_SAMPLES, setSamples } from "../actions/samples";

const loadSamplesFlow =
  ({ api }) =>
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    if (action.type === LOAD_SAMPLES) {
      try {
        const samples = await api.samples.getAll();
        dispatch(setSamples(samples));
      } catch (error) {
        console.log(error);
      }
    }
  };

export default [loadSamplesFlow];
